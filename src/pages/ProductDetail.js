import {Link, useParams} from 'react-router-dom';
import { useEffect, useState } from "react";
import axios from 'axios';
import "../App.css";
import { FaStar } from 'react-icons/fa';
import Product from "../components/product/Product";

const API = 'https://api.lemonilo.com/v1/'
const PRODUCT = 'product/'

const ProductDetail = (props) => {

    const [product, setProduct] = useState();
    const params = useParams();
    const { product_slug } = params

    console.log('params', params);
    const getProductDetail = async () => {
        try {
            console.log('product_slug', product_slug)
            const { data } =  await axios.get(API + PRODUCT + product_slug)
            setProduct(data.data)
            console.log('data', data)
        } catch (e) {
            // silent e
        }
    }
    useEffect(() => {
        getProductDetail();
    }, [])
    console.log('product', product);
    return (
        <>
        <div className="container mx-auto jarak">
            <div className="card w-full flex flex-row" >
                <div className="flex-1">
                    <img src={product?.productImages[0]['photoUrl']} alt="Avatar" style={{ width: 470, height: 'auto' }} />
                </div>
                <div className="flex-1">
                    <div>
                        <p className='p-merchant'>{product?.merchant?.name}</p>
                        <h4 className='p-name'><b>
                            <span>{product?.name}</span> <br/>
                        </b></h4>
                        <p>{product?.actualWeight * 1000} gr</p>
                        <div className="flex flex-row">
                            <div className="flex-shrink price">
                                 <span style={{ textDecoration: 'line-through', color:  product?.gimmickPrice ? 'red' : 'gray'}}>
                                     Rp. {product?.gimmickPrice}
                                </span>
                            </div>
                            <div className="flex-1 price">
                               Rp. {product?.finalSellPrice}
                            </div>
                        </div>

                        <h4 className='promo'>PROMO KHUSUS APP</h4>
                        <br />
                        <hr />
                        <br />

                        <h3><b>Stok Banyak!</b>Tersisa >50 !</h3>
                        <button className='button-beli'>Beli</button>
                    </div>
                </div>
            </div>
            <hr/>
            <h2 className='sub-judul'>Rincian Produk</h2>
            <table>
                <tr>
                <td className='rincian'>Tanggal Kadaluarsa</td>
                <td>{product?.expirationStartDate}</td>
                </tr>
                <tr>
                <td className='rincian'>Berat Bersih</td>
                <td>{product?.netWeight} Kg</td>
                </tr>
                <tr>
                <td className='rincian'>Berat Paket</td>
                <td>{product?.actualWeight} Kg</td>
                </tr>
                <tr>
                <td className='rincian'>Dimensi Paket</td>
                <td>{product?.height} X {product?.width} X {product?.width} CM</td>
                </tr>
            </table>
        <hr/>

        <h2 className='sub-judul'>Deskripsi Produk</h2>
        <b>{product?.description}</b>
        <hr/>
        <h2 className='sub-judul'>Ulasan Produk</h2>
        <p className='reviewaverage'><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/>
        <span className='reviewaverage-1'>{product?.productScore?.averageStar} / 5</span></p>
            <div className='reviewbox'>
            <div className='reviewaveragesmall'><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/></div>
            <p>"{product?.reviews[0]?.message}"</p>
            <p>Anonymous</p>
            <p>{product?.reviews[0]?.publishedAt}</p>  
            </div>
            <div className='reviewbox'>
            <div className='reviewaveragesmall'><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/></div>
            <p>"{product?.reviews[1]?.message}"</p>
            <p>Anonymous</p>
            <p>{product?.reviews[1]?.publishedAt}</p>
            
            </div>
            <div className='reviewbox'>
            <div className='reviewaveragesmall'><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/><FaStar style={{color:'yellow'}}/></div>
            <p>"{product?.reviews[2]?.message}"</p>
            <p>Anonymous</p>
            <p>{product?.reviews[2]?.publishedAt}</p>
            
            </div>
        
        
        </div>
        </>
    )
};

export default ProductDetail;