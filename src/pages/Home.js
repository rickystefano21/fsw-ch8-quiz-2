import Product from "../components/product/Product";
import ReactPaginate from "react-paginate";
import React, {useEffect, useState} from "react";
import axios from "axios";
import Sidebar from "../components/sidebar/sidebar"
import "../App.css";

const API = 'https://api.lemonilo.com/v1/'
const PRODUCT_POPULAR = 'product/popular?'
const Home = () => {
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(true);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPage, setTotalPage] = useState(1);
    const fetchProducts  = async (query = '') => {
        try {
            const { data } = await axios.get(API + PRODUCT_POPULAR + query);
            setCurrentPage(data?.currentPage)
            setTotalPage(data?.totalPages)
            setProducts(data?.data || []);
            setLoading(false);
        } catch (e) {
            setLoading(false)
            // silent e
        }
    }
    useEffect(() => {
        fetchProducts()
    }, [])

    const addToCart = () => {

    }
    useEffect(() => {
        fetchProducts(`page=${currentPage}`)
    }, [currentPage])

    const handlePageClick = (value) => {
        setCurrentPage(value.selected + 1)
    }
    return (
        <>
        <div className="home">
        <Sidebar/>
            <Product products={products} addToCart={addToCart} />
            </div>
            <div className="position">
            <ReactPaginate
                breakLabel="..."
                nextLabel="next >"
                onPageChange={handlePageClick}
                pageRangeDisplayed={5}
                pageCount={totalPage}
                previousLabel="< previous"
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakClassName="page-item"
                breakLinkClassName="page-link"
                containerClassName="pagination"
                activeClassName="active"
                renderOnZeroPageCount={null}
            />
            </div>
        </>
    )
}

export default Home;