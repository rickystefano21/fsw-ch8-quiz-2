import React, { Component, useState, useEffect } from 'react'
import './App.css';
import { useNavigate, Navigate } from "react-router-dom";
import {
    Routes,
    Route,
} from "react-router-dom";
import Home from "./pages/Home";
import ProductDetail from "./pages/ProductDetail";
import Login from './pages/Login'
import Profile from "./pages/Profile";
import routes from "./routes";
import Footer from "./components/footer/footer"
import Header from "./components/header/header"
import Sidebar from './components/sidebar/sidebar';

const NotFound = () => {
    return (
        <div>
            Not Found
        </div>
    )
}

export default (props) => {
    let navigate = useNavigate();
    
    const user = localStorage.getItem('user')
      return (
        <>
          <Header/>
          <div>
                        <Routes>
                  {routes.map((route) => {
                      return (
                          <Route key={route.path} path={route.path} element={route.page}/>
                      )
                  })}
                  {/*<Route path="/" element={<Home />} />*/}
                  {/*<Route path="product/:product_slug" element={<ProductDetail />} />*/}
                  {/*<Route path="profile" element={*/}
                  {/*    <ProtectedRouteAuth user={user}>*/}
                  {/*        <Profile></Profile>*/}
                  {/*    </ProtectedRouteAuth>*/}
                  {/*} />*/}
                  {/*<Route path="auth/login" element={<ProtectedRouteNonAuth user={user}> <Login /> </ProtectedRouteNonAuth>}/>*/}
                  <Route path="*" element={<NotFound />} />
              </Routes>
              <Footer/>
          </div>
          </>
      )
}


// class App extends Component{
//   constructor() {
//     super();
//
//     this.testButton = this.testButton.bind(this);
//   }
//   state = {
//     product: [],
//     loading: true,
//     productsAdded: [],
//     test: false
//   }
//
//   componentDidMount() {
//     this.fetchProductData();
//   }
//
//   async fetchProductData () {
//     try {
//       const { data } = await axios.get(API);
//       this.setState({
//         product: data.data.map((d) => ({
//           ...d,
//           quantityToBuy: 0
//         })),
//         loading: false
//       })
//     } catch (e) {
//       this.setState({
//         loading: false
//       })
//       // silent e
//     }
//   }
//
//
//
//   addToCart (index) {
//     let product = this.state.product.map((product, i) => ({
//       ...product,
//       quantityToBuy: i === index ? product.quantityToBuy += 1 : 0
//     }));
//
//     this.setState({
//       product
//     });
//
//     // alert('anda berhasil menambahkan barang');
//   }
//
//   componentDidUpdate(prevProps, prevState) {
//   }
//
//   testButton () {
//     this.setState({
//       test: !this.state.test
//     })
//   }
//
//   render() {
//     const { product, loading } = this.state;
//     console.log('is being rerender');
//     return (
//         <div className="App">
//           <div>
//             <ul>
//               {loading ? (
//                   <div>Loading ....</div>
//               ): (
//                   <div>
//                     { !product.length ? (
//                         <div>No Data Provided</div>
//                     ): (
//                         <Product testButton={() => this.testButton()} product={product}/>
//                     )}
//                   </div>
//               )}
//             </ul>
//           </div>
//         </div>
//     );
//   }
// }
//
// export default React.memo(App);
