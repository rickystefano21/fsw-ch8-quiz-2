import React from "react";
import './Product.css';
import { Link } from "react-router-dom";

const Product = ({ products, addToCart }) => {
    return (
        <div className="product flex flex-row w-full flex-wrap space-x-6 justify-between">
            {products.map((product, index) => (
                <div className="card w-3/12" key={index}>
                    <Link to={`product/${product.slug}`}>
                        <img src={product?.photoUrl} alt="Avatar" style={{ width: '100%' }} />
                        <div>
                        <p>Lemonilo</p>
                        <p className="tag-best">Best Seller</p>
                        <p className="discount">-{product.discount}%</p>
                            <h4><b>
                                <span>{product.name}</span> <br/>
                            </b></h4>
                            <p>{product.actualWeight} Kg</p>
                            <p>
                                <span style={{ textDecoration: 'line-through', color:  product.gimmickPrice ? 'red' : 'gray' }}>
                                     Rp. {product.gimmickPrice}
                                </span>
                            </p>
                            <p className="price">Rp. {product.price}</p>
                        </div>
                    </Link>
                    <button className="buy-button" onClick={() => alert(index)}>Buy</button>
                </div>
            ))}
        </div>
    )
}

export default React.memo(Product);