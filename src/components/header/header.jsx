import React from "react";
import './header.css';
import { Link } from "react-router-dom";
import { useNavigate, Navigate } from "react-router-dom";
import logo from './logo.svg';
import SearchBar from "../seacrh-bar/search-bar";

const Header = () => {
    let navigate = useNavigate();
      
    return (
        <>
        <div className="navbar-set">
        <div className="topheader">
            <div className="Pro">Download Aplikasi Lemonilo</div>
            <div className="Pro">Butuh Bantuan? (+62) 21 4020 0788</div>
            <div className="Pro">Diskon Sebesar 5000 Untuk Pengguna Baru</div>
            <div className="Pro">Kenapa Belanja di Lemonilo?</div>   
        </div>

        <div className="navbar">
            <div className="">
                <div className="" onClick={() => {
                    navigate('/')
                        }}>
                <h1>LEMONILO</h1>
                </div>
            </div>
            <div className="kategori">
                <p className="p-kategori">Kategori</p>
                <p className="p-kategori">Promo</p>
                <p className="p-kategori">Life</p>
            </div>
            <SearchBar/>  
            <div className="login">
                <p className="p-login">Login</p>
                <p className="p-login">Sign up</p>
            </div>
        </div>
        </div>
              </>
    )
}
export default Header;