import React from "react";
import './footer.css';
import { Link } from "react-router-dom";

const footer = () => {
    return (
        <div>
            <p className="footer">Copyright © 2016-2022 PT. Lemonilo Indonesia Hebat. All rights reserved.</p>
        </div>
    )
}

export default footer;