import React from "react";
import './search-bar.css';
import { Link } from "react-router-dom";


const SearchBar = () => (
    <form action="/" method="get">
            <span className="visually-hidden">Cari Produk Apa?</span>
        <input
            type="text"
            id="header-search"
            placeholder="Cari Produk Apa?"
            name="s" 
        />
        <button type="submit">Cari</button>
    </form>
);

export default SearchBar;