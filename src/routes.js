import { Navigate } from "react-router-dom";
import Home from "./pages/Home";
import ProductDetail from "./pages/ProductDetail";
import Profile from "./pages/Profile";
import Login from "./pages/Login";

const ProtectedRouteNonAuth = ({ children }) => {
    const user = localStorage.getItem('user')
    if(user) {
        return <Navigate to="/" replace />;
    }
    return children;
};
const ProtectedRouteAuth = ({ children }) => {
    const user = localStorage.getItem('user')
    if(!user) {
        return <Navigate to="/auth/login" replace />;
    }
    return children;
}

const routes = [
    {
        path: '/',
        page: <Home />
    },
    {
        path: 'product/:product_slug',
        page: <ProductDetail />
    },
    {
        path: 'profile',
        page: (
            <ProtectedRouteAuth>
                <Profile />
            </ProtectedRouteAuth>
        )
    },
    {
        path: 'auth/login',
        page: (
            <ProtectedRouteNonAuth>
                <Login />
            </ProtectedRouteNonAuth>
        )
    }
]

export default routes